<?php
/**
 * Created by PhpStorm.
 * User: OKS
 * Date: 22.02.2018
 * Time: 14:55
 */
use kartik\tree\TreeView;
use oks\categories\models\Categories;
use oks\langs\components\Lang;
?>

<?php
echo \oks\langs\widgets\LangsWidgets::widget();
echo TreeView::widget([
    // single query fetch to render the tree
    // use the Product model you have in the previous step
    'query' => Categories::find()->lang()->addOrderBy('root, lft'),
    'headingOptions' => ['label' => Yii::t('oks-categories','Categories')],
    'fontAwesome' => true,
    'isAdmin' => false,
    'displayValue' => 1,
    'softDelete' => true,
    'cacheSettings' => [
        'enableCache' => true
    ],
    'nodeAddlViews' => [
        \kartik\tree\Module::VIEW_PART_2 => '@vendor/oks/yii2-categories/src/views/_treeManagerBottomPlace',
    ]
]);