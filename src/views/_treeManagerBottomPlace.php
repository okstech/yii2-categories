<?php
/**
 * Created by PhpStorm.
 * User: OKS
 * Date: 22.02.2018
 * Time: 16:32
 */
echo $form->field($node, 'description')->textarea();

if($node->isRoot())
{
    echo $form->field($node, 'type')->dropDownList(\oks\categories\models\Categories::find()->allTypes());
}

echo $form->field($node, 'slug');