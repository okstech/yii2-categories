<?php
namespace oks\categories\behaviors;

/**
 *
 * @author OKS Technologies info@oks.uz
 *
 */

use oks\categories\models\Categories;
use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

class CategoryQueryBehavior extends Behavior
{
    public function category($slug = null){
        $this->owner->joinWith(['categories']);
        if($slug !== null){
            $this->owner->where(['categories.slug' => $slug]);
        }
        return $this->owner;
    }
}