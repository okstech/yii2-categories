<?php
namespace oks\categories\actions;

use Yii;
use yii\base\Action;

class CategoriesAction extends Action{

    public function run(){

        return $this->controller->render('@vendor/oks/yii2-categories/src/views/categories');
    }

}